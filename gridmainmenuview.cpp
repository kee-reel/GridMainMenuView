#include "gridmainmenuview.h"

GridMainMenuView::GridMainMenuView() :
	PluginBase(this),
	m_GUIElementBase(new GUIElementBase(this, {"MainMenu"}, "qrc:/Menu.qml"))
{
	initPluginBase(
	{
		{INTERFACE(IPlugin), this},
		{INTERFACE(IGUIElement), m_GUIElementBase}
	});
	m_elements.reset(new ReferenceInstancesList<IGUIElement>());
	m_GUIElementBase->initGUIElementBase(
		{{"elements", m_elements.data()}},
		{},
		{{"MainMenuItem", m_elements}}
	);

	connect(m_elements.data(), &ReferenceInstancesList<IGUIElement>::instancesChanged, [=]() {
		QMetaObject::invokeMethod(m_GUIElementBase->rootObject(), "recreateMenuItems");
	});
}
