#pragma once


#include <QtCore>

#include "../../Common/Plugin/plugin_base.h"
#include "../../Common/GUIElement/guielementbase.h"


//! \addtogroup GUIManager_dep
//!  \{
class GridMainMenuView : public QObject, public PluginBase
{
	Q_OBJECT
    Q_PLUGIN_METADATA(IID "PLAG.PLAGins.GridMainMenuView" FILE "PluginMeta.json")
	Q_INTERFACES(
		IPlugin
	)

public:
	explicit GridMainMenuView();
	~GridMainMenuView() override = default;
	
private:
	GUIElementBase* m_GUIElementBase;
	ReferenceInstancesListPtr<IGUIElement> m_elements;
};
//!  \}

